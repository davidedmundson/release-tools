#!/usr/bin/env python3

from setuptools import find_packages, setup


setup(
    name='kde-release',
    version='1.0',
    description='Utilities of the KDE Release team',
    packages=find_packages(),
    python_requires='>=3.6',
    install_requires=[
        'beautifulsoup4 >=4.6.0, <5',
        'click >=7, <8',
        'GitPython >=2.1.14, <3',
        'requests >=2.18.2, <3',
    ],
    entry_points={
        'console_scripts': [
            'add-bugzilla-versions = kde_release.cli:add_bugzilla_versions',
            'add-appstream-versions = kde_release.cli:add_appstream_versions',
        ],
    },
)
