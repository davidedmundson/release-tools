#!/bin/bash

. config

# Make sure gpg-remote is running
gpg --digest-algo SHA512 --armor --detach-sign -o /dev/null -s config || exit 2

cat modules.git | while read repoid branch; do
(
    bash pack.sh $repoid $1
)
done

# for sending to release-team@kde.org
cat versions/* > REVISIONS_AND_HASHES
